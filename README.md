﻿#to run the jar file

./run.sh

#to execute the test cases
Insight-Digital-Payment /insight_testsuite/run_tests.sh

#
Main file : Insight-Digital-Payment/src/AntiFraud.java ( Uses Bidirectional-BFS to speedup graph search for feature 2 and feature 3)
#
JUnit file : Insight-Digital-Payment /test/AntiFraudTest.java

#
Other files : 
CreateDataBase.java : File to read batch_payment.txt and create a graph structure
UserNode.java : Class to hold the UserID and transaction Nodes
