
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class AntiFraud {
	static File fout =null;
	static HashMap<Integer,Set<UserNode>> unique = new HashMap<Integer,Set<UserNode>>();
	
	public static void main(String[] args) throws FileNotFoundException
	{
		long startTime = System.currentTimeMillis();
		final String dir = System.getProperty("user.dir");
		String csvFile = dir+"\\paymo_input\\batch_payment.csv";
		System.out.println(csvFile);
		CreateDataBase.createGraph(csvFile,unique);
		long endTime = System.currentTimeMillis();
		System.out.println("Total time: " + (endTime-startTime));
		csvFile = dir+"\\paymo_input\\stream_payment.csv";
		
		String path = dir+"\\paymo_output\\";
		startTime = System.currentTimeMillis();
		int count=findIfFraud(csvFile,new File(path+"output1.txt"),unique,1);
		endTime = System.currentTimeMillis();
		System.out.println(count);
		System.out.println("Average time per query in ms: " + String.format("%.4f",(double)(endTime-startTime)/count));
		startTime = System.currentTimeMillis();
		count=findIfFraud(csvFile,new File(path+"output2.txt"),unique,2);
		endTime = System.currentTimeMillis();
		System.out.println("Average time per query in ms: " + String.format("%.4f",(double)(endTime-startTime)/count));
		startTime = System.currentTimeMillis();
		count=findIfFraud(csvFile,new File(path+"output3.txt"),unique,3);
		endTime = System.currentTimeMillis();
		System.out.println("Average time per query in ms: " + String.format("%.4f",(double)(endTime-startTime)/count));
	
	}

	static int findIfFraud(String csvFile, File fout, HashMap<Integer,Set<UserNode>> unique, int feature) throws FileNotFoundException
	{
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		String line = "";
        String cvsSplitBy = ",";
        int count=0;
        File file = new File(csvFile);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
            	if(count==0)
            	{
            		count++;
            		continue;
            	}
            	
                String[] entry = line.trim().split(cvsSplitBy);
                if(entry.length >3 && entry[1].matches("[0-9 ]+") && entry[2].matches("[0-9 ]+"))
                {
                	UserNode to = new UserNode(Integer.valueOf(entry[1].replaceAll("\\s","")));
                   	UserNode from = new UserNode(Integer.valueOf(entry[2].replaceAll("\\s","")));

                   	switch(feature)
                   	{
                   	case 1:
                   		bw.write(isFraud_single(to,from,bw,unique));
                   		bw.newLine();
                   		break;
                   	case 2:
                   		bw.write(isFraud_multi(to,from,bw,2,unique));
                   		bw.newLine();
                   		break;
                   	case 3:
                   		bw.write(isFraud_multi(to,from,bw,4,unique));
                   		bw.newLine();
                   		break;
                   	}        	
                	count++;	
                	
                }
//                
          }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return count;
 
	}
	
	
	
	
	static String isFraud_single(UserNode from, UserNode to, BufferedWriter bw,HashMap<Integer,Set<UserNode>> unique) throws IOException
	{
		if(!unique.containsKey(to.getID()) || !unique.containsKey(from.getID()))
		{
			
			return "unverified" ;
		}
			Set<UserNode> tn = unique.get(from.getID());
		 if(tn.contains(to))
			 {
			 return "trusted";
			 }
		 else
		 	{
			 return "unverified";
		 	}	
	}
	// Use bi-directional BFS to find 4th degree connection
	static String isFraud_multi(UserNode from, UserNode to, BufferedWriter bw,int limit,HashMap<Integer,Set<UserNode>> unique) throws IOException
	{
		if(!unique.containsKey(to.getID()) || !unique.containsKey(from.getID()))
		{
			return "unverified";
		}
		
		    Queue<UserNode> fromQ = new LinkedList<UserNode>();
		    Queue<UserNode> toQ = new LinkedList<UserNode>();
		    Set<Integer> visitedFrom = new HashSet<Integer>();
		    Set<Integer> visitedTo = new HashSet<Integer>();

		    fromQ.add(from);
		    visitedFrom.add(from.getID());
		    toQ.add(to);
		    visitedTo.add(to.getID());
		    boolean friendFound=true;
		    int countFrom=0,countTo=0;
		    while(!fromQ.isEmpty() && !toQ.isEmpty())
		    {
		    	UserNode user = fromQ.poll();
				Set<UserNode> friends = unique.get(user.getID());
				for ( UserNode friend : friends)
				{
					
					if(visitedTo.contains(friend.getID()))
					{
						friendFound=true;
						if(countFrom + countTo+1  <=limit)
						{
							return ("trusted");
//							
						}
						else
						{
							return "unverified";
						}
						
					}
					if(!visitedFrom.contains(friend.getID()))
							{
								visitedFrom.add(friend.getID());
								fromQ.add(friend);
								
							}
						
				}
				
				countFrom++;
				user = toQ.poll();
				friends = unique.get(user.getID());
				for ( UserNode friend : friends)
				{
					
					if(visitedFrom.contains(friend.getID()))
					{
						friendFound=true;
						if(countFrom + countTo+1  <=limit)
						{
							return "trusted";
						}
						else
						{
							return "unverified";
						}
					}
					if(!visitedTo.contains(friend.getID()))
							{
								visitedTo.add(friend.getID());
								toQ.add(friend);
								
							}
						
				}
				countTo++;
				if(countTo+ countFrom >2 && friendFound==false)
				{
					return "unverified";
				}
				
		    }
		    return null;
	}
	

// degree of seperation using BFS
//	static void isFraud2(UserNode from, UserNode to, BufferedWriter bw) throws IOException
//	{
//		if(!unique.containsKey(to.getID()) || !unique.containsKey(from.getID()))
//		{
//			bw.write("unverified");
//			bw.newLine();
//			return;
//		}
//		Queue<UserNode> q = new LinkedList<UserNode>();
//		Set<Integer> visited = new HashSet<>();
//		q.add(from);
//		visited.add(from.getID());
//		int count =0;
//		boolean friendFound = false;
//		while(!q.isEmpty())
//		{
//			UserNode user = q.poll();
//			Set<UserNode> friends = unique.get(user.getID());
//			for ( UserNode friend : friends)
//			{
//				
//				if(friend.getID()== to.getID())
//				{
//					friendFound=true;
//					if(count +1 <=2)
//					{
//						bw.write("trusted");
//						bw.newLine();
////						System.out.println("trusted:"+(count+1));
//						return;
//					}
//					else
//					{
//						bw.write("unverified");
//						bw.newLine();
//					}
//					
//				}
//				if(!visited.contains(friend.getID()))
//						{
//							visited.add(friend.getID());
//							q.add(friend);
//							
//						}
//					
//			}
//			
//			count++;
//			if(count >2 && friendFound==false)
//			{
//				bw.write("unverified");
//				bw.newLine();
////				System.out.println("unverified:"+count);
//				break;
//			}
//			
//		}
//		
//	}

	
}
