import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class UserNode{

	private int ID; 
	private HashMap<Integer,UserNode> transactions;
	UserNode(int ID)
	{
		this.ID = ID;
		this.transactions = new HashMap<Integer,UserNode>();
	}
	public int getID() {
		return ID;
	}
	
	
	
	public HashMap<Integer,UserNode> getTransactions() {
		return this.transactions;
	}
	public void setID(int id) {
		ID = id;
	}
	public void setTransactions(HashMap<Integer,UserNode> transactions) {
		this.transactions.putAll(transactions);
	}
	
	public void addTransaction(UserNode newUser) {
		transactions.put(newUser.getID(), new UserNode(newUser.getID()));
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserNode other = (UserNode) obj;
		if (ID != other.ID)
			return false;
		return true;
	}
	
	
	
	
	
	
	
	
}
