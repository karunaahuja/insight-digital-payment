
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CreateDataBase{

	static Map<Integer,Set<UserNode>> unique = new HashMap<Integer,Set<UserNode>>();

	@SuppressWarnings("null")
	public static void createGraph(String csvFile, HashMap<Integer,Set<UserNode>> unique)
	{
		String line = "";
        String cvsSplitBy = ",";
        int count=0;
        File file=new File(csvFile);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
            	if(count==0)
            	{
            		count++;
            		continue;
            	}
            	
                String[] entry = line.trim().split(cvsSplitBy);
                if(entry.length >3 && entry[1].matches("[0-9 ]+") && entry[2].matches("[0-9 ]+"))
                {
                	count++;
                	UserNode from = new UserNode(Integer.valueOf(entry[1].replaceAll("\\s","")));
                   	UserNode to = new UserNode(Integer.valueOf(entry[2].replaceAll("\\s","")));
                	
                   Set<UserNode> tn;
                   	if(!unique.containsKey(from.getID()))
                	{
                		
                		tn = new HashSet<UserNode>();
                		
                  	}
                   	else
                   	{
                   		tn = unique.get(from.getID());
                   	}
                   	tn.add(to);
                   	unique.put(from.getID(),tn);
                   	
                   	if(!unique.containsKey(to.getID()))
                	{
                		
                		tn = new HashSet<UserNode>();
                		
                  	}
                   	else
                   	{
                   		tn = unique.get(to.getID());
                   	}
                   	tn.add(from);
                   	unique.put(to.getID(),tn);
                		
                }
//                System.out.pr/intln("done");
          }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("count " +count);
	}
	
	

}
