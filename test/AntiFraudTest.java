import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class AntiFraudTest extends TestCase{
   protected HashMap<Integer,Set<UserNode>> unique = new HashMap<Integer,Set<UserNode>>();
   String csvFile;

   @Before
   protected void setUp(){
	   final String dir = System.getProperty("user.dir");
	   csvFile = dir+"\\test\\batch_payment.txt";
	   CreateDataBase.createGraph(csvFile, unique);
   }

   @Test
   public void testFeature1() throws IOException{
	   final String dir = System.getProperty("user.dir");
	   String path1 = dir+"\\test\\Feature1\\" ;
	   String csvFile1 = path1 + "stream_payment.txt"; 
	   File outputFile1 = new File(path1+"\\output.txt");
	   AntiFraud.findIfFraud(csvFile1,outputFile1,unique,1);
	   try(BufferedReader br = new BufferedReader(new FileReader(outputFile1))) {
		    String line = br.readLine();
		    assertTrue(line.equals("unverified"));
		    line = br.readLine();
		    assertTrue(line.equals("trusted"));
		}
   }

   @Test
   public void testFeature2() throws IOException{
	   final String dir = System.getProperty("user.dir");
	   String path = dir+"\\test\\Feature2\\" ;
	   String csvFile = path + "stream_payment.txt"; 
	   File outputFile = new File(path+"\\output.txt");
	   AntiFraud.findIfFraud(csvFile,outputFile,unique,2);
	   
	   try(BufferedReader br = new BufferedReader(new FileReader(outputFile))) {
		    String line = br.readLine();
		    assertTrue(line.equals("trusted"));
		    line = br.readLine();
		    assertTrue(line.equals("unverified"));
		}
   }
   @Test
   public void testFeature3() throws IOException{
	   final String dir = System.getProperty("user.dir");
	   String path = dir+"\\test\\Feature3\\" ;
	   String csvFile = path + "stream_payment.txt"; 
	   File outputFile = new File(path+"\\output.txt");
	   AntiFraud.findIfFraud(csvFile,outputFile,unique,3);
	   
	   try(BufferedReader br = new BufferedReader(new FileReader(outputFile))) {
		    String line = br.readLine();
		    assertTrue(line.equals("trusted"));
		    line = br.readLine();
		    assertTrue(line.equals("trusted"));
		}
   }

}